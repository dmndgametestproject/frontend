const {ccclass, property} = cc._decorator;

@ccclass
export default class SoundOnClick extends cc.Component {

    @property(cc.AudioClip)
    sound: cc.AudioClip = null;

    active = true;

    onLoad () {
        this.node.on(cc.Node.EventType.MOUSE_DOWN,(e:cc.Event.EventMouse)=>{
            if (this.active) {
                cc.audioEngine.play(this.sound, false, 1);
            }
        });
    }

}
