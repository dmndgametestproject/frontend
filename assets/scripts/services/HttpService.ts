import Config from "./Config";

export default class HttpService {
    static get(endpoint, searchParams = {}) {
        let searchArray = [];
        for (let key in searchParams) {
            searchArray.push(`${key}=${searchParams[key]}`);
        }
        let searchString = searchArray.length > 0 ? '?'+searchArray.join('&') : '';

        return new Promise((resolve, reject) => {
            var request = cc.loader.getXMLHttpRequest();
            request.open("GET", Config.api_url+endpoint+searchString, true);
            request.setRequestHeader("Content-Type","application/json;charset=UTF-8");
            request.onreadystatechange = () => {
                if (request.readyState === 4) {
                    if(request.status === 200) {
                        var data = JSON.parse(request.responseText);
                        resolve(data)
                    } else {
                        reject("Request failed");
                    }
                }
            }
            request.send();
        });
    }

    static post(endpoint, body) {

        return new Promise((resolve, reject) => {
            var request = cc.loader.getXMLHttpRequest();
            request.open("POST", Config.api_url+endpoint, true);
            request.setRequestHeader("Content-Type","application/json;charset=UTF-8");
            request.onreadystatechange = () => {
                if (request.readyState == 4) {
                    if (request.status === 200) {
                        var data = JSON.parse(request.responseText);
                        resolve(data)
                    } else {
                        reject("Request failed");
                    }
                }
            }
            request.send(JSON.stringify(body));
        });
    }
}