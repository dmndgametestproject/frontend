export default class Config {
    static api_url = 'http://localhost:3000/';
    static segment_params = {
        min:1000,
        max:100000,
        count:16,
        val:100,
        interval:1000
    }
}