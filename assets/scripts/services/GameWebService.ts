import HttpService from "./HttpService";

export default class GaeWebService {
    static async spin(score) {
        let result = await HttpService.post('spin', {score});
        return Number.parseInt(result["score"]);
    }

    static async getScore() {
        let result = await HttpService.get('score');
        return Number.parseInt(result["score"]);
    }

    static async getSegments(params) {
        return await HttpService.get('segments');
    }
}