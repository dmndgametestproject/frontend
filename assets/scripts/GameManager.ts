const {ccclass, property} = cc._decorator;

@ccclass
export default class GameManager extends cc.Component {

    @property(cc.Node)
    startScreen: cc.Node = null;

    @property(cc.Node)
    wheelScreen: cc.Node = null;

    start () {
        this.startScreen.active = true;
        this.wheelScreen.active = false;
    }

    startGame () {
        this.startScreen.active = false;
        this.wheelScreen.active = true;
    }
}
