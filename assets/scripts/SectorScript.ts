const {ccclass, property} = cc._decorator;

@ccclass
export default class SectorScript extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    value: number;

    init (angle:number, value: number) {
        this.node.rotation = angle;
        this.value = value;
        this.label.string = value.toString();
    }
}
