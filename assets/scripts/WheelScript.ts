import SectorScript from "./SectorScript";
import ScoreScript from "./ScoreScript";
import SoundOnClick from "./SoundOnClick";
import GameWebService from "./services/GameWebService";
import Config from "./services/Config";

const {ccclass, property} = cc._decorator;

@ccclass
export default class WheelScript extends cc.Component {

    @property(cc.Node)
    sectorNode: cc.Node = null;

    @property(ScoreScript)
    scoreNode: ScoreScript = null;

    @property(cc.AudioClip)
    coinSound: cc.AudioClip = null;

    @property(cc.Button)
    spinButton: cc.Button = null;

    @property()
    slowing = 0.5;
    @property()
    speed = 300;
    @property()
    border = 50;

    private count = 16;

    private actSlowing = 0;
    private actSpeed = 0;
    private actBorder = 0;

    private ready = false;

    private sectorNodes : SectorScript[] = [];

    onLoad () {

        for (let i = 1; i < this.count; i++) {
            var _sectorNode = cc.instantiate(this.sectorNode);

            _sectorNode.parent = this.node;
        }
    }

    async start() {
        this.sectorNodes = this.node.getComponentsInChildren(SectorScript);

        let segments = await GameWebService.getSegments(Config.segment_params);
        for (let i = 0; i < this.count; i++) {
            this.sectorNodes[i].init(360/this.count*i, segments[i]);
        }
        let score = await GameWebService.getScore();

        this.scoreNode.setScore(score);

        this.ready = true;
    }

    spin () {
        if (this.ready) {
            this.actSpeed = this.speed + this.speed*Math.random();
            let diff = this.actSpeed / this.speed;

            this.actSlowing = this.slowing / diff;
            this.actBorder = this.border * diff;

            this.ready = false;
            this.spinButton.interactable = false;
            this.spinButton.node.getComponents(SoundOnClick).forEach(c  => c.active = false);
        }
    }

    async update (dt) {
        if (this.actSpeed !== 0) {
            this.node.rotation += this.actSpeed * dt
            if (this.node.rotation > 360) {
                this.node.rotation -= 360;
            }

            this.actSpeed *= 1 - (1-this.actSlowing) * dt;
            if ( this.actSpeed < this.actBorder) {
                this.actSpeed = 0;

                let data = await GameWebService.spin(this.findWin())
                
                this.scoreNode.setScore(data);

                this.ready = true;
                this.spinButton.interactable = true;
                this.spinButton.node.getComponents(SoundOnClick).forEach(c  => c.active = true);

                cc.audioEngine.play(this.coinSound, false, 1);
               // })
            }
        }
    }

    findWin() {
        for (let i = 0; i < this.sectorNodes.length; i++) {
            let normal = 360/this.count*i;
            let _left = normal - 360/this.count/2;
            let _right = normal + 360/this.count/2;

            //fix for first
            if (_left < 0 && 360-this.node.rotation > _right) {
                _left += 360;
                _right += 360;
            }

            if (360-this.node.rotation > _left && 360-this.node.rotation < _right) {
                return this.sectorNodes[i].value;
            }
        }
        return 0
    }
}
