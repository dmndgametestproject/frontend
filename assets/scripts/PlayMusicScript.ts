const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayMusicScript extends cc.Component {

    @property(cc.AudioClip)
    music: cc.AudioClip = null;

    private musicId: number;

    onLoad () {
        this.play();
    }

    onDestroy() {
        if (this.musicId) {
            cc.audioEngine.stop( this.musicId);
        }
    }

    play () {
        this.musicId = cc.audioEngine.playMusic(this.music, true)
    }

    stop () {
        cc.audioEngine.stop(this.musicId);
    }
}
