const {ccclass, property} = cc._decorator;

@ccclass
export default class ScoreScript extends cc.Component {

    label: cc.Label = null;

    value: number = 0;

    onLoad () {
        this.label = this.getComponent(cc.Label);
    }

    setScore (value:number) {
        this.value = value;
        this.label.string = this.formatScore(this.value);
    }

    private formatScore(value:number) {
        if (value > 1000000)
            return Math.round(value / 1000000 * 100) / 100 + 'm';
        else if (value > 1000)
            return Math.round(value / 1000 * 100) / 100 + 'k';
        else
            return value.toString();
    }

}
